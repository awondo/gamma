'use strict';

// Photos controller
angular.module('photos').controller('PhotosController', ['$scope', '$stateParams', '$location', 'Authentication', 'Photos',
  function($scope, $stateParams, location, Authentication, Photos ) {
    $scope.authentication = Authentication;

    $(document).ready(function() {
      if (window.location.hash == "#!/photos/create") {
        $(document).scrollTop(276);
      }
      $(".fancybox-thumb").fancybox({

        prevEffect  : 'fade',
        nextEffect  : 'fade',
        helpers : {
          title : {
            type: 'inside'
          },
          thumbs  : {
            width : 50,
            height  : 50
          }
        }

      });

    });

    $(document).ready(function(){
      if (Modernizr.touch) {
                    // show the close overlay button
                    $(".close-overlay").removeClass("hidden");
                    // handle the adding of hover class when clicked
                    $(".img").click(function(e){
                      if (!$(this).hasClass("hover")) {
                        $(this).addClass("hover");
                      }
                    });
                    // handle the closing of the overlay
                    $(".close-overlay").click(function(e){
                      e.preventDefault();
                      e.stopPropagation();
                      if ($(this).closest(".img").hasClass("hover")) {
                        $(this).closest(".img").removeClass("hover");
                      }
                    });
                  } else {
                    // handle the mouseenter functionality
                    $(".img").mouseenter(function(){
                      $(this).addClass("hover");
                    })
                    // handle the mouseleave functionality
                    .mouseleave(function(){
                      $(this).removeClass("hover");
                    });
                  }
                });

            // Create new Photo
            $scope.create = function() {
                // Create new Photo object
      
                var photo = new Photos ({
                  name: this.name
                });

                // Redirect after save
                photo.$save(function(response) {
                  $location.path('photos/' + response._id);

                    // Clear form fields
                    $scope.name = '';
                  }, function(errorResponse) {
                    $scope.error = errorResponse.data.message;
                  });
              };

            // Remove existing Photo
            $scope.remove = function( photo ) {
              if ( photo ) { photo.$remove();

                for (var i in $scope.photos ) {
                  if ($scope.photos [i] === photo ) {
                    $scope.photos.splice(i, 1);
                  }
                }
              } else {
                $scope.photo.$remove(function() {
                  $location.path('photos');
                });
              }
            };


            // Update existing Photo
            $scope.update = function() {
              var photo = $scope.photo ;
                photo.$update(function() {
                $location.path('photos/' + photo._id);
              }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
              });
            };

            // Find a list of Photos
            $scope.find = function() {
              $scope.photos = Photos.query();
            };

            // Increase thevote total of a photo by 1
            $scope.upvote = function($inputphoto) {
                  
                  $inputphoto.$update();
                
                };

            // Find existing Photo
            $scope.findOne = function() {
              $scope.photo = Photos.get({ 
                photoId: $stateParams.photoId
              });
            };
          }
      ]);
      


angular.module('upload-modal',['ui.bootstrap']);
var uploadphotoController = function($scope, $modal, $log){

  $scope.items = ['item1', 'item2', 'item3'];

  $scope.openUploadModal = function (size) {

    var modalInstance = $modal.open({
      templateUrl: 'upload-photo-modal.html',
      controller: ModalInstanceCtrl,
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
    });
  };
  
  $scope.openSignIn = function (size) {

    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: ModalInstanceCtrl,
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
    });
  };
};

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

var ModalInstanceCtrl = function ($scope, $modalInstance, items) {

  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
};

// modal for voting below
angular.module('user-not-signed-in-modal',['ui.bootstrap']);
var openVoteModal = function($scope, $modal, $log){

  $scope.items = ['item1', 'item2', 'item3'];

  $scope.openVote = function (size) {

    var modalInstance = $modal.open({
      templateUrl: 'vote-modal.html',
      controller: ModalInstanceCtrl,
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
    });
  };
  
  $scope.openSignIn = function (size) {

    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: ModalInstanceCtrl,
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
    });
  };
};

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

var ModalInstanceCtrl = function ($scope, $modalInstance, items) {

  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
};


