'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('photos');

//angular.module('Alpha', ['ngRoute', 'angularFileUpload']);

angular.module('myApp', ['ngRoute']).
config(['$routeProvider', function($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/home'});
}]);

