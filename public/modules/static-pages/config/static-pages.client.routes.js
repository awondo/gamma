'use strict';

//Setting up route
angular.module('static-pages').config(['$stateProvider',
	function($stateProvider) {
		// Static pages state routing
		$stateProvider.
		state('contact', {
			url: '/contact',
			templateUrl: 'modules/static-pages/views/contact.client.view.html'
		}).
		state('about', {
			url: '/about',
			templateUrl: 'modules/static-pages/views/about.client.view.html'
		}).
		state('static-pages', {
			url: '/sponsors',
			templateUrl: 'modules/static-pages/views/sponsors.client.view.html'
		});
	}
]);