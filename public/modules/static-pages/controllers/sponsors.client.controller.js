'use strict';

angular.module('static-pages').controller('SponsorsController', ['$scope',
	function($scope) {
		// Controller Logic
		// ...
	  $(document).ready(function() {
	  $(window).scrollTop();
	  $(document).scrollTop();
    $(".fancybox")
      .attr('rel', 'gallery')
      .fancybox({
          padding : 0,
          autoHeight: true,
          afterLoad : function() {
            this.content = this.content.html();     
          }
      });
    });
	
	}
]);

angular.module('plunker', ['ui.bootstrap']);
function AlertDemoCtrl($scope) {
  $scope.alerts = [
  ];

  $scope.addAlert = function() {
    $scope.alerts.push({type: 'success', msg: 'Thanks! We will contact you soon!'});
  };
  // close function, scroll to the top slowly..
  $scope.closeAlert = function(index) {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $scope.alerts.splice(index, 1);
  };

}
