'use strict';

module.exports = function (app) {
    var globalPath;
    var users = require('../../app/controllers/users');
    var photos = require('../../app/controllers/photos');
    var fs = require('fs-extra');
    var easyimg = require('easyimage');
    var gm = require('gm');
    // Photos Routes

    app.route('/photos')
        .get(photos.list)
        .post(users.requiresLogin, photos.create);

    app.route('/photos/:photoId')
        .get(photos.read)
        .put(users.requiresLogin, photos.hasAuthorization, photos.update)
        .delete(users.requiresLogin, photos.hasAuthorization, photos.delete);

    // Finish by binding the Photo middleware
    app.param('photoId', photos.photoByID);


    var busboy = require('connect-busboy');
    //...

    app.use(busboy());
    //What we should use...
    app.post('/fileupload', function (req, res) {
        var s3 = require('s3');

        var client = s3.createClient({
            maxAsyncS3: 20,     // this is the default
            s3RetryCount: 3,    // this is the default
            s3RetryDelay: 1000, // this is the default
            multipartUploadThreshold: 20971520, // this is the default (20 MB)
            multipartUploadSize: 15728640, // this is the default (15 MB)
            s3Options: {
                accessKeyId: "AKIAJNK3JCBBZMZGF6KQ",
                secretAccessKey: "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",
                // any other options are passed to new AWS.S3()
                // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property
            },
        });

        var fstream;
        console.log('in uploader for 2222' + process.cwd());
        var timeStamp = new Date().getTime();
        var filename = req.user.username + req.pipe(req.busboy);
        var newFileName = req.user._id + timeStamp;
        var extension = ".";
        //console.log("The file name is  " + filename);
        req.busboy.on('file', function (fieldname, file, filename) {
            console.log("File is " + filename);
            var tempName = filename.toString();
            console.log(tempName);
            extension = extension + tempName.split('.').pop();
            console.log("extension is ::" + extension);
            var path = __dirname + "/" + newFileName;
            globalPath = path;
            fstream = fs.createWriteStream(path);
            file.pipe(fstream);
            /////////CALL A FUNCTION OR ADD DIRECTLY TO THE PHOTO DB
            var username = req.user.username;
            var userId = req.user._id;
            var mongoose = require('mongoose'),
                Photo = mongoose.model('Photo'),
                _ = require('lodash');

            //console.log(Photo);

            fs.copy(__dirname + "/" + newFileName, __dirname + "/" + newFileName + extension, function(err){
                if (err) return console.error(err);
                console.log("success!")

                easyimg.rescrop({
                 src:__dirname + "/" + newFileName, dst:__dirname + "/" + newFileName +"copy"+ "temp" + "thumb" + extension,
                 width:280, height:280,
                 cropwidth:280, cropheight:280,
                 x:0, y:0
              }).then(
              function(image) {
                 console.log('Resized and cropped: ' + image.width + ' x ' + image.height);
              },
              function (err) {
                console.log(err);
              }
            );





            })
            


            
        

            fstream.on('close', function () {
                var params = {
                    localFile: __dirname + "/" + newFileName,

                    s3Params: {
                        Bucket: "photoappseng123.com",
                        Key: "userphotos/" + newFileName + extension,
                        // other options supported by putObject, except Body and ContentLength.
                        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
                    },
                };

                var uploader = client.uploadFile(params);

                uploader.on('error', function (err) {
                    console.error("unable to upload:", err.stack);
                });

                uploader.on('progress', function () {
                    console.log("progress", uploader.progressMd5Amount,
                        uploader.progressAmount, uploader.progressTotal);
                });
                uploader.on('end', function () {
                    console.log("done uploading");
                    


                });
                var params2 = {
                    localFile: __dirname + "/" + newFileName + "copy" + "temp" + "thumb" + extension,

                    s3Params: {
                        Bucket: "photoappseng123.com",
                        Key: "userphotos/" + newFileName + "copy" +"temp" + "thumb" + extension,
                        // other options supported by putObject, except Body and ContentLength.
                        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
                    },
                };

                var uploader2 = client.uploadFile(params2);

                uploader2.on('error', function (err) {
                    console.error("unable to upload:", err.stack);
                });

                uploader2.on('progress', function () {
                    console.log("progress", uploader.progressMd5Amount,
                        uploader.progressAmount, uploader.progressTotal);
                });
                uploader.on('end', function () {
                    console.log("done uploading");
                    console.log(req.user._id);

                    new Photo({
                        theme: "Around the World",
                        userID: "req.user.username",
                        URL: "https://s3-ap-southeast-2.amazonaws.com/photoappseng123.com/userphotos/" + newFileName + extension,
                        thumbURL: "https://s3-ap-southeast-2.amazonaws.com/photoappseng123.com/userphotos/" + newFileName + "copy" +"temp" + extension
                    }).save(function (err, doc) {
                            if (err) res.json(err);
                            //else    res.send('Successfully inserted!');
                        });

                });
            });





//                var fs = require('fs');
//
//                fs.unlinkSync(__dirname + "/" + newFileName)
//                console.log('successfully deleted /tmp/hello');
//                var response = {
//                    status: 'File was uploaded successfuly!',
//                };
            res.redirect('/#!/photos');




        });

    });
};
