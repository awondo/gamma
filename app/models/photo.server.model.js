'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Photo Schema
 */
var PhotoSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	URL: {
		type: String,
		default: '',
		trim: true
	},
	votes: {
		type: Number,
		default: 0,
		trim: true
	},
    theme: {
        type: String,
        default: '',
        trim: true
    },
    voters: {
        type: Array,
        default: [],
        trim: true
    },
    username: {
        type: Array,
        default: [],
        trim: true
    },
	userID: {
		type: String,
        trim: true
    }
});

mongoose.model('Photo', PhotoSchema);